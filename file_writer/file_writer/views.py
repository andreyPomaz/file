from django.shortcuts import render

def file_writer(request):
    return render(request, 'file_writer/file_writer.html')