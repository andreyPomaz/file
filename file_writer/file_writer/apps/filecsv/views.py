import os
from .tasks import first_task
# import pandas as pd
# import pandas
from MySQLdb.converters import NoneType
from django.conf import settings
# from django.contrib.auth.models import User
from django.shortcuts import render, redirect
import csv, io
from django.contrib import messages
from django.core.paginator import Paginator
# from django.http import HttpResponse, HttpResponseRedirect
# from django.urls import reverse
from numpy.f2py.auxfuncs import isinteger

from .models import FileProduct, fileModel
from django.contrib.auth.decorators import permission_required

from .forms import FileForm


@permission_required('admin.can_add_log_entry')
def data_upload(request):
    template = "file_upload.html"

    if request.method == 'GET':
        return render(request, template)

    with open(os.path.join('file_writer', 'media/', 'files/example-file_9727WA4_1_1Xaou41.csv')) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            id = row['id']
            MSKU = row['MSKU']
            ASIN = row['ASIN']
            UPC = row['UPC']
            MPN = row['MPN']
            supplier_map_price = row['supplier_map_price']
            adjusted_supplier_unit_price = row['adjusted_supplier_unit_price']
            supplier_unit_price = row['supplier_unit_price']
            supplier_pack_size = row['supplier_pack_size']
            supplier_sold_by_increments = row['supplier_sold_by_increments']
            supplier_case_size = row['supplier_case_size']
            supplier_notes = row['supplier_notes']
            supplier_item_description = row['supplier_item_description']
            supplier_unit_shipping_weight_in_pounds = row['supplier_unit_shipping_weight_in_pounds']
            supplier_status_code = row['supplier_status_code']
            misc1 = row['misc1']
            misc2 = row['misc2']
            misc3 = row['misc3']
            misc4 = row['misc4']
            misc5 = row['misc5']
            supplier_sku = row['supplier_sku']
            supplier_upcharge_pct = row['supplier_upcharge_pct']
            supplier_backorder_qty = row['supplier_backorder_qty']
            brand = row['brand']
            MSRP = row['MSRP']
            updated_at = row['updated_at']
            supplier_id = row['supplier_id']
            uploaded_at = row['uploaded_at']
            case_upc = row['case_upc']
            category_code = row['category_code']
            shelf_life = row['shelf_life']

            new_file_example = FileProduct(
                base_id=id, MSKU=MSKU, ASIN=ASIN, UPC=UPC, MPN=MPN,
                supplier_map_price=supplier_map_price, adjusted_supplier_unit_price=adjusted_supplier_unit_price,
                supplier_unit_price=supplier_unit_price, supplier_pack_size=supplier_pack_size,
                supplier_sold_by_increments=supplier_sold_by_increments, supplier_case_size=supplier_case_size,
                supplier_notes=supplier_notes, supplier_item_description=supplier_item_description,
                supplier_unit_shipping_weight_in_pounds=supplier_unit_shipping_weight_in_pounds,
                supplier_status_code=supplier_status_code, misc1=misc1, misc2=misc2,
                misc3=misc3, misc4=misc4, misc5=misc5, supplier_sku=supplier_sku,
                supplier_upcharge_pct=supplier_upcharge_pct, supplier_backorder_qty=supplier_backorder_qty, brand=brand,
                MSRP=MSRP,
                updated_at=updated_at, supplier_id=supplier_id, uploaded_at=uploaded_at, case_upc=case_upc,
                category_code=category_code, shelf_life=shelf_life
            )
            new_file_example.save()
    context = {}
    return render(request, template, context)
    # template = "file_upload.html"
    #
    # if request.method == 'GET':
    #     return render(request, template)
    #
    # csv_file = request.FILES['file']
    # if not csv_file.name.endswith('.csv'):
    #     messages.error(request, 'Please upload a .csv file.')
    #
    # data_set = csv_file.read().decode('UTF-8')
    # io_string = io.StringIO(data_set)
    # next(io_string)
    #
    # for column in csv.reader(io_string, delimiter=',', quotechar="|"):
    #     _, created = File.objects.update_or_create(
    #         MSKU=column[0],
    #         ASIN=column[1],
    #         UPC=column[2],
    #         MPN=column[3],
    #         supplier_map_price=column[4],
    #         adjusted_supplier_unit_price=column[5],
    #         supplier_unit_price=column[6],
    #         supplier_pack_size=column[7],
    #         supplier_sold_by_increments=column[8],
    #         supplier_case_size=column[9],
    #         supplier_notes=column[10],
    #         supplier_item_description=column[11],
    #         supplier_unit_shipping_weight_in_pounds=column[12],
    #         supplier_status_code=column[13],
    #         misc1=column[14],
    #         misc2=column[15],
    #         misc3=column[16],
    #         misc4=column[17],
    #         misc5=column[18],
    #         supplier_sku=column[19],
    #         supplier_upcharge_pct=column[20],
    #         supplier_backorder_qty=column[21],
    #         brand=column[22],
    #         MSRP=column[23],
    #         updated_at=column[24],
    #         supplier_id=column[25],
    #         uploaded_at=column[26],
    #         case_upc=column[27],
    #         category_code=column[28],
    #         shelf_life=column[29],
    #     )
    # context = {}
    # return render(request, template, context)


def File(request):
    if request.method == 'POST':
        form = FileForm(request.POST, request.FILES)
        csv_file = request.FILES['file']
        if form.is_valid():
            user_id = form.cleaned_data["user_id"].id
            file = csv_file
            path = "http://127.0.0.1:8000/base/" + str(1) + "/"
            status_id = settings.STATUSES[0]
            name = request.FILES['file'].name
            type_id = os.path.splitext(str(csv_file))[1]
            first_task.delay(user_id, file, path, status_id, name, type_id, csv_file, request)

    form = FileForm()
    return render(request, 'show_base.html', {"form": form})


def showFile(request):
    if request.method == "GET":
        source = fileModel.objects.all()
        return render(request, 'Base[show_file].html', {"source": source})


def untill_eleven(request, id):
    if request.method == "GET":
            data = FileProduct.objects.filter(file_id=id)
            paginator = Paginator(data, 100)
            page = request.GET.get('page')
            data = paginator.get_page(page)

            return render(request, 'Base.html', {'all_data': data, 'id': id})
#     context = {"csv_rows": []}
#     with open(os.path.join('file_writer', 'media/', 'files/example-file_9727WA4_1_1Xaou41.csv')) as csvfile:
#         reader = csv.reader(csvfile)
#         for row in reader:
#             context["csv_rows"].append(" ".join(row))
#     return render(request, 'Base.html', context)
