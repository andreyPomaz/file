from file_writer.celery import app
from .models import FileProduct, fileModel
import csv
import os
from django.contrib import messages
from django.conf import settings
from .forms import FileForm
from django.contrib.auth.models import User

@app.task
def first_task(user_id, file, path, status_id, name, type_id, csv_file, request):
        newdoc = fileModel(
            user_id=User.objects.get(id=user_id),
            file=request.FILES['file'],
            path=path,
            status_id=status_id,
            name=name,
            type_id=type_id

        )
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'Please upload a .csv file.')
            newdoc.status_id = "Failed"
            newdoc.save()
        else:
            newdoc.save()
            newdoc.path = "http://127.0.0.1:8000/base/" + str(newdoc.id) + "/"
            newdoc.save()
            with open(os.path.join('file_writer', 'media/', str(newdoc.file))) as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    id = row['id']
                    supplier_code = row['supplier_code']
                    if row['supplier_stock_level'].replace('.', '', 1).isdigit():
                        supplier_stock_level = row['supplier_stock_level']
                    else:
                        supplier_stock_level = "have to be number!"
                    supplier_backorder_eta = row['supplier_backorder_eta']
                    MSKU = row['MSKU']
                    ASIN = row['ASIN']
                    if row['UPC'].replace('.', '', 1).isdigit():
                        UPC = row['UPC']
                    else:
                        UPC = "have to be number!"
                    MPN = row['MPN']
                    supplier_map_price = row['supplier_map_price']
                    if row['adjusted_supplier_unit_price'].replace('.', '', 1).isdigit():
                        adjusted_supplier_unit_price = row['adjusted_supplier_unit_price']
                    else:
                        adjusted_supplier_unit_price = "have to be number!"
                    if row['supplier_unit_price'].replace('.', '', 1).isdigit():
                        supplier_unit_price = row['supplier_unit_price']
                    else:
                        supplier_unit_price = "have to be number!"
                    if row['supplier_pack_size'].replace('.', '', 1).isdigit():
                        supplier_pack_size = row['supplier_pack_size']
                    else:
                        supplier_pack_size = "have to be number!"
                    if row['supplier_sold_by_increments'].replace('.', '', 1).isdigit():
                        supplier_sold_by_increments = row['supplier_sold_by_increments']
                    else:
                        supplier_sold_by_increments = "have to be number!"
                    if row['supplier_case_size'].replace('.', '', 1).isdigit():
                        supplier_case_size = row['supplier_case_size']
                    else:
                        supplier_case_size = "have to be number!"
                    supplier_notes = row['supplier_notes']
                    supplier_item_description = row['supplier_item_description']
                    supplier_unit_shipping_weight_in_pounds = row['supplier_unit_shipping_weight_in_pounds']
                    supplier_status_code = row['supplier_status_code']
                    misc1 = row['misc1']
                    misc2 = row['misc2']
                    misc3 = row['misc3']
                    misc4 = row['misc4']
                    misc5 = row['misc5']
                    if row['supplier_sku'] is None or row['supplier_sku'].replace('.', '', 1).isdigit():
                        supplier_sku = row['supplier_sku']
                    else:
                        supplier_sku = "have to be number!"
                    supplier_upcharge_pct = row['supplier_upcharge_pct']
                    supplier_backorder_qty = row['supplier_backorder_qty']
                    brand = row['brand']
                    if row['MSRP'] is None or row['MSRP'].replace('.', '', 1).isdigit():
                        MSRP = row['MSRP']
                    else:
                        supplier_sku = "have to be number!"
                    updated_at = row['updated_at']
                    supplier_id = row['supplier_id']
                    uploaded_at = row['uploaded_at']
                    case_upc = row['case_upc']
                    category_code = row['category_code']
                    shelf_life = row['shelf_life']

                    new_file_example = FileProduct(
                        file_id=newdoc.id,
                        base_id=id,
                        supplier_backorder_eta=supplier_backorder_eta, supplier_stock_level=supplier_stock_level,
                        supplier_code=supplier_code,
                        MSKU=MSKU, ASIN=ASIN, UPC=UPC, MPN=MPN,
                        supplier_map_price=supplier_map_price,
                        adjusted_supplier_unit_price=adjusted_supplier_unit_price,
                        supplier_unit_price=supplier_unit_price, supplier_pack_size=supplier_pack_size,
                        supplier_sold_by_increments=supplier_sold_by_increments, supplier_case_size=supplier_case_size,
                        supplier_notes=supplier_notes, supplier_item_description=supplier_item_description,
                        supplier_unit_shipping_weight_in_pounds=supplier_unit_shipping_weight_in_pounds,
                        supplier_status_code=supplier_status_code, misc1=misc1, misc2=misc2,
                        misc3=misc3, misc4=misc4, misc5=misc5, supplier_sku=supplier_sku,
                        supplier_upcharge_pct=supplier_upcharge_pct, supplier_backorder_qty=supplier_backorder_qty,
                        brand=brand, MSRP=MSRP,
                        updated_at=updated_at, supplier_id=supplier_id, uploaded_at=uploaded_at, case_upc=case_upc,
                        category_code=category_code, shelf_life=shelf_life
                    )
                    newdoc.status_id = "completed"
                    newdoc.save()
                    new_file_example.save()
