from django.apps import AppConfig


class FilecsvConfig(AppConfig):
    name = 'filecsv'
