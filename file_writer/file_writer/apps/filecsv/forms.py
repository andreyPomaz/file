from django import forms

from .models import fileModel


class FileForm(forms.ModelForm):
    class Meta:
        model = fileModel
        fields = ['file', 'user_id']
